@ProjectManagement @PoolPartyTest1

  Feature: Create Project
           validate created project

   #UC01
   @CreateProjectPPAR @SmokeTest @SanityTest

   Scenario: Create a project with given ppar file and validate the result
     Given A valid ppar file are available
     When when I create a project with ppar file
     Then User should receive a status code of 200
     And I validate the project title in response
     And store the project ID