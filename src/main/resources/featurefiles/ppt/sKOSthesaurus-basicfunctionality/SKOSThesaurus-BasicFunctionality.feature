@SKOSThesaurusBasicFunctionality

Feature: Work with SKOS Thesaurus basic functionality
         i.e: Fetch, Create, delete Concept Scheme, Concept(Narrower concept, Broader concept)....

  #UC01
  @getConceptSchemes

  Scenario: get the list of all concept schemes in the given project
    Given A PoolParty project Id is provided
    When I sent the request with project ID
    Then The list of concept schemes should be returned
    And User should receive a status code of 200
