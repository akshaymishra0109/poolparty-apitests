@PPX

  Feature: Heartbeat PPX API

    #UC01
    @Heartbeat
    Scenario: Checks the connection to the index and returns false if something is wrong
      Given A server is available and Index is configured
      And Default user sends request
      When User sends a request with heartbeat API
      Then User should receive a status code of 200


    #UC02
    @Heartbeat
    Scenario: Checks the connection to the index and returns false if something is wrong
      Given A server is available and Index is configured
      When User sends a request with wrong credentials
      Then User should receive a status code of 401