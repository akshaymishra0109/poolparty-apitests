@PPX

Feature: File Extract PPX API

  #UC01
  @FileExtract
  Scenario: Sending English pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_EN_TXT.pdf" in format "application/pdf" to extract file service using language en
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains concept with prefLabelLanguage en, prefLabelValue "State", conceptUrl http://eurovoc.europa.eu/883, score 100.0, frequencyInDocument 173
    And File extraction result contains extracted term with extractedTerm "member", score 100.0, frequencyInDocument 216
#    And Verify details of entryIndex 0 of concept results for extraction: prefLabelLanguage "en", prefLabelValue "State", conceptUrl "http://eurovoc.europa.eu/883", score 100.0, frequencyInDocument 173

  #UC02
  @FileExtract
  Scenario: Sending German pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_DE_TXT.pdf" in format "application/pdf" to extract file service using language de
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains extracted term with extractedTerm "artikel", score 100.0, frequencyInDocument 238
    And File extraction result contains concept with prefLabelLanguage "de", prefLabelValue "Richtlinie", conceptUrl "http://eurovoc.europa.eu/448", score 100.0, frequencyInDocument 129

  #UC03
  @FileExtract
  Scenario: Sending Bulgarian pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_BG_TXT.pdf" in format "application/pdf" to extract file service using language bg
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains extracted term with extractedTerm "на", score 100.0, frequencyInDocument 1337
    And File extraction result contains concept with prefLabelLanguage "bg", prefLabelValue "директива", conceptUrl "http://eurovoc.europa.eu/448", score 100.0, frequencyInDocument 119

  #UC04
  @FileExtract
  Scenario: Sending Spanish pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_ES_TXT.pdf" in format "application/pdf" to extract file service using language es
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains extracted term with extractedTerm "navegación", score 100.0, frequencyInDocument 186
    And File extraction result contains concept with prefLabelLanguage "es", prefLabelValue "Directiva", conceptUrl "http://eurovoc.europa.eu/448", score 100.0, frequencyInDocument 117

  #UC05
  @FileExtract
  Scenario: Sending French pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_FR_TXT.pdf" in format "application/pdf" to extract file service using language fr
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains extracted term with extractedTerm "l’union", score 100.0, frequencyInDocument 167
    And File extraction result contains concept with prefLabelLanguage "fr", prefLabelValue "directive", conceptUrl "http://eurovoc.europa.eu/448", score 100.0, frequencyInDocument 117

  #UC06
  @FileExtract
  Scenario: Sending Greek pdf file with text content to Extractor using project Eurovoc 4.6 and expecting correct and complete extraction results
    Given Extraction model for Eurovoc 4.6 is available
    And Default user sends request
    When User sends "data/testData/ppxFiles/CELEX_32017L2397_EL_TXT.pdf" in format "application/pdf" to extract file service using language el
    Then User should receive a status code of 200
    And Content type is of response type Json-UTF8
    And User should get exactly 25 extracted concepts from file extraction
    And User should get exactly 25 extracted terms from file extraction
    And Result should match expected schema "jsonSchema/ppx/extraction/extractFile_concepts_ExtractedTerms.json"
    And File extraction result contains extracted term with extractedTerm "και", score 100.0, frequencyInDocument 485
    And File extraction result contains concept with prefLabelLanguage "el", prefLabelValue "χώρα μέλος", conceptUrl "http://eurovoc.europa.eu/2298", score 100.0, frequencyInDocument 143
