@PPX

Feature: Extraction of metadata like concepts and terms from Text

   #TC1
   @TextExtraction
   Scenario Outline: Extracts and returns meaningful metadata like concepts and terms from a given text
    Given Project Id is provided and Index is properly configured
    And Default user sends request
    When User sends the request for text <text> and language <lang>
    Then User should receive a status code of <statuscode>
    And User should get exactly <Nrconcepts> extracted concepts
    And User should get exactly <Nrterms> extracted terms


     Examples:
       | text          | lang | statuscode |Nrconcepts|Nrterms|
       | textEnglish   | en   | 200        |25        |25     |
       | textGerman    | de   | 200        |25        |25     |
       | textSpanish   | es   | 200        |25        |25     |
       | textFrench    | fr   | 200        |25        |25     |
#       | textGreek     | el   | 200        |25        |25     |  disabled due to special character issue
#       | textBulgarian | bg   | 200        |25        |25     |

   #TC2
   @TextExtraction
   Scenario Outline: Extracts and returns meaningful metadata like concepts and terms  with  limit from a given text
     Given Project Id is provided and Index is properly configured
     And Default user sends request
     When User sends the request with limit of concept <Nrconcepts> and terms <Nrterms> for text <text> and language <lang>
     Then User should receive a status code of <statuscode>
     And User should get exactly <Nrconcepts> extracted concepts
     And User should get exactly <Nrterms> extracted terms

     Examples:
       | Nrconcepts | Nrterms | text          | lang | statuscode |
       | 1          | 1       | textEnglish   | en   | 200        |
       | 1          | 1       | textGerman    | de   | 200        |
       | 1          | 1       | textSpanish   | es   | 200        |
       | 1          | 1       | textFrench    | fr   | 200        |
       | 1          | 1       | textGreek     | el   | 200        |
#       | 1          | 1       | textBulgarian | bg   | 200        |   disabled due to special character issue

  #TC3 - Negative scenario for negative value of concept
  @TextExtraction
  Scenario Outline: Check the response code in case of negative value of noofconcept parameter
    Given Project Id is provided and Index is properly configured
    And Default user sends request
    When User sends the request with limit of concept <Nrconcepts> and terms <Nrterms> for text <text> and language <lang>
    Then User should receive a status code of <statuscode>

    Examples:
      | Nrconcepts | Nrterms | text          | lang | statuscode |
      | -1         | 1       | textEnglish   | en   | 400        |
      | -1         | 1       | textGerman    | de   | 400        |
      | -1         | 1       | textSpanish   | es   | 400        |
      | -1         | 1       | textFrench    | fr   | 400        |
      | -1         | 1       | textGreek     | el   | 400        |
      | -1         | 1       | textBulgarian | el   | 400        |


  #TC4 - Negative scenario for negative value of term
  # disabling due to (defect P3D-1702)  TODO
#  @TextExtraction
#  Scenario Outline: Check the response code in case of negative value of noofterms parameter
#    Given Project Id is provided and Index is properly configured
#    And Default user sends request
#    When User sends the request with limit of concept <Nrconcepts> and terms <Nrterms> for text <text> and language <lang>
#    Then User should receive a status code of <statuscode>
#
#    Examples:
#      | Nrconcepts | Nrterms | text          | lang | statuscode |
#      | 1          | -1      | textEnglish   | en   | 400        |
#      | 1          | -1      | textGerman    | de   | 400        |
#      | 1          | -1      | textSpanish   | es   | 400        |
#      | 1          | -1      | textFrench    | fr   | 400        |
#      | 1          | -1      | textGreek     | el   | 400        |
#      | 1          | -1      | textBulgarian | el   | 400        |


    #TC5
    @TextExtraction
    Scenario Outline: Extracts and returns only meaningful terms from a given text
      Given Project is not provided and Index is correctly configured
      And Default user sends request
      When User sends the request for text extraction with text <text> and  language <lang>
      Then User should receive a status code of <statuscode>
      And User should get only extracted terms but no extracted concepts

      Examples:
        | text          | lang | statuscode |
        | textEnglish   | en   | 200        |
        | textGerman    | de   | 200        |
        | textSpanish   | es   | 200        |
        | textFrench    | fr   | 200        |
        | textGreek     | el   | 200        |
        | textBulgarian | bg   | 200        |


  #TC6 - Negative scenario for incorrect language
  # disabling due to (defect P3D-1695) TODO
#  @TextExtraction
#  Scenario Outline: Check the response code in case of invalid language
#    Given Project Id is provided and Index is properly configured
#    And Default user sends request
#    When User sends the request for text <text> and incorrect language <lang>
#    Then User should receive a status code of <statuscode>
#
#
#    Examples: Invalid
#      | text          | lang     | statuscode |
#      | textEnglish   | language | 400        |
#      | textGerman    | language | 400        |
#      | textSpanish   | language | 400        |
#      | textFrench    | language | 400        |
#      | textGreek     | language | 400        |
#      | textBulgarian | language | 400        |


