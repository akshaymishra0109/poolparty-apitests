@PPX

Feature: Extracts from url

  Extracts and returns meaningful metadata like concepts and terms from a given url

  GET /api/extract
  [url] Extracts and returns meaningful metadata like concepts and terms from a given url

  API Documentation: https://test-pp-linux.semantic-web.at/extractor/api
  Test data sets: https://ourgrips.semantic-web.at/display/PPQ/Extract+test+data+sets

  #UC0100 | Happy path
  @UrlExtract
  Scenario Outline: Extract meaningful metadata from a given url
    Given Project to get concepts and terms from a given url is available
    And Default user sends request
    When User sends api call with language <language> and Url <url> to get concepts and terms
    Then User should receive a status code of <status_code>
    And User should get exactly <extractedConcepts> extracted concepts
    And User should get exactly <extractedTerms> extracted terms
    And Content type is of response type Json-UTF8

    Examples: Happy paths
      | language | extractedTerms | extractedConcepts | status_code | url                                                                      |
      | en       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
      | de       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32017L2397 |
      | es       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32017L2397 |
      | fr       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32017L2397 |
      | el       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/EL/TXT/HTML/?uri=CELEX:32017L2397 |
      | bg       | 25             | 25                | 200         | http://eur-lex.europa.eu/legal-content/BG/TXT/HTML/?uri=CELEX:32017L2397 |

  #UC0200 | Unhappy path
  @UrlExtract
  Scenario Outline: Try to extract meaningful metadata from a given url using wrong parameters data
    Given Project to get concepts and terms from a given url is available
    And Default user sends request
    When User sends api call with language <language> and Url <url> to get concepts and terms
    Then User should receive a status code of <status_code>

    Examples: Unhappy paths
      | language | status_code | url           |
      | en       | 400         | iAmInvalidUrl |

      #P3D-1695/bug //TODO
      #| lngx     | 400         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |

  #UC0300 | Unhappy path
  @UrlExtract
  Scenario Outline: Try to extract from a given url using non existing project
    Given Project to get concepts and terms from a given url is available
    And Default user sends request
    When User sends api call with language <language> and Url <url> using non existing project
    Then User should receive a status code of <status_code>

    Examples: Unhappy path for non existing project
      | language | status_code | url                                                                      |
      | en       | 400         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |

  #UC0400 | Happy path
  @UrlExtract
  Scenario Outline: Extract limited number of meaningful metadata from a given url by setting number of concepts/terms
    Given Project to get concepts and terms from a given url is available
    And Default user sends request
    When User sends api call with language <language>, Url <url> with limit for concepts <limitConcepts> and terms <limitTerms>
    Then User should receive a status code of <status_code>
    And User should get exactly <limitConcepts> extracted concepts
    And User should get exactly <limitTerms> extracted terms

    Examples: Happy paths
      | language | limitConcepts | limitTerms | status_code | url                                                                      |
      | en       | 1             | 1          | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
      | bg       | 6             | 12         | 200         | http://eur-lex.europa.eu/legal-content/BG/TXT/HTML/?uri=CELEX:32017L2397 |

  #UC0500 | Happy path
  @UrlExtract
  Scenario Outline: Extract limited number of meaningful metadata from a given url by setting concept minimum score
    Given Project to get concepts and terms from a given url is available
    And Default user sends request
    When User sends api call with language <language>, Url <url> with limit for concept minimum score of <conceptMinimumScore>
    Then User should receive a status code of <status_code>
    # Add verification that all extracted concepts have score bigger than conceptMinimumScore test value //TODO
    # Adapt "extractedConcepts" values when P3D-1465/bug will be deployed //TODO

    Examples: Happy paths
      | language | conceptMinimumScore | status_code | url                                                                      |
      | en       | 75.0                | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
      | bg       | 6.0                 | 200         | http://eur-lex.europa.eu/legal-content/BG/TXT/HTML/?uri=CELEX:32017L2397 |

 # Build tests //TODO
 # #UC0600 | Happy path
 # @UrlExtract
 # Scenario Outline: Extract meaningful metadata from a given url and check extracted terms
 #   Given Project to get concepts and terms from a given url is available
 #   And Default user sends request
 #   When User sends api call with language <language> and Url <url> to get concepts and terms
 #   Then I should receive a status code of <status_code>
 #   And Extracted term of entryIndex <index> is <textValue> with score <score> and frequency <frequencyInDocument>
 #
 #   Examples: Happy paths
 #     | language | index | textValue        | score | frequencyInDocument | status_code | url                                                                      |
 #     | en       | 0     | member           | 100.0 | 216                 | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
 #     | en       | 4     | union            | 64.0  | 136                 | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
 #     | bg       | 0     | на               | 100.0 | 1294                | 200         | http://eur-lex.europa.eu/legal-content/BG/TXT/HTML/?uri=CELEX:32017L2397 |
 #     | es       | 5     | estados miembros | 76.0  | 134                 | 200         | http://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32017L2397 |
 #
 #
 # #UC0700 | Happy path
 # @UrlExtract
 # Scenario Outline: Extract meaningful metadata from a given url and check extracted concepts
 #   Given Project to get concepts and terms from a given url is available
 #   And Default user sends request
 #   When User sends api call with language <language> and Url <url> to get concepts and terms
 #   Then I should receive a status code of <status_code>
 #   And Extracted concept of entryIndex <index> in language <language> is <prefLabels> with id <id>, uri <uri>, score <score> and frequency <frequencyInDocument>
 #
 #   # order of concepts in response is not consistent, so verification step has to be adapted //TODO
 #   Examples: Happy paths
 #     | language | index | prefLabels   | id                            | uri                           | score | frequencyInDocument | status_code | url                                                                      |
 #     | en       | 0     | State        | http://eurovoc.europa.eu/883  | http://eurovoc.europa.eu/883  | 100.0 | 173                 | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
 #     | en       | 18    | insured risk | http://eurovoc.europa.eu/3728 | http://eurovoc.europa.eu/3728 | 12.0  | 19                  | 200         | http://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32017L2397 |
 #     | de       | 13    | Datenbasis   | http://eurovoc.europa.eu/4821 | http://eurovoc.europa.eu/4821 | 11.0  | 15                  | 200         | http://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32017L2397 |