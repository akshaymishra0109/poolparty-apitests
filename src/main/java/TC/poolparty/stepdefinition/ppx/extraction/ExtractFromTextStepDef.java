package TC.poolparty.stepdefinition.ppx.extraction;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import com.poolparty.commonutils.BasePaths;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;
import testdata.ExtractorTextData;
import java.lang.reflect.Field;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ExtractFromTextStepDef extends CommonObject {

    private CommonObject base;
    public ExtractFromTextStepDef(CommonObject base){
        this.base=base;
    }

    // common method to get provided long text
    public String getText(String text) throws Exception {

        ExtractorTextData extractorTextData = new ExtractorTextData();
        Field field = extractorTextData.getClass().getField(text);

        Object fetchText = field.get(extractorTextData);
        String getText = fetchText.toString();

        return getText;

    }

    // common method to build a basic request with language, projectID, text
    public RequestSpecification buildBasicRequest(String text, String lang,String projectId){

        RequestSpecification httpRequest = RestAssured.given().header("Content-Type", "application/x-www-form-urlencoded")
                                            .formParam("text",text).formParam("language",lang).formParam("projectId",projectId);

        return httpRequest;
    }

    // #TC1
    @Given("Project Id is provided and Index is properly configured")
    public void projectIdIsProvidedAndIndexIsproperlyConfigured() throws Exception {

        base.projectIdPpx = BaseSetup.properties.getPropertyValue("projectIdPpx");

    }


    @When("User sends the request for text (.*) and language (.*)")
    public void userSendsTheRequestForTextTextAndLanguageLang(String text, String lang) throws Exception {

        String getText = getText(text);

        RequestSpecification httpRequest = buildBasicRequest(getText,lang,base.projectIdPpx);

        base.response = httpRequest.request(Method.POST, BasePaths.EXTRACTOR_EXTRACT);

    }

    // #TC2 , #TC3 , #TC4
    @When("User sends the request with limit of concept (.*) and terms (.*) for text (.*) and language (.*)")
    public void userSendsTheRequestWithLimitOfConceptConceptsAndTermsTermsForTextTextAndLanguageLang(int concepts,int terms, String text, String lang) throws Exception {

        String getText = getText(text);

        RequestSpecification httpRequest = buildBasicRequest(getText,lang,base.projectIdPpx).formParam("numberOfConcepts",concepts).formParam("numberOfTerms",terms);

        base.response = httpRequest.request(Method.POST, BasePaths.EXTRACTOR_EXTRACT);

    }


    // #TC5
    @Given("Project is not provided and Index is correctly configured")
    public void projectIsNotProvidedAndIndexIsCorrectlyConfigured() {
    }

    @When("User sends the request for text extraction with text (.*) and  language (.*)")
    public void userSendsTheRequestForTextExtractionWithTextTextAndLanguageLang(String text, String lang) throws Exception {

        String getText = getText(text);

        RequestSpecification httpRequest = RestAssured.given().formParam("text",getText).formParam("language",lang);

        base.response = httpRequest.request(Method.POST, BasePaths.EXTRACTOR_EXTRACT);

    }


    // #TC6
    @When("User sends the request for text (.*) and incorrect language (.*)")
    public void userSendsTheRequestForTextTextAndIncorrectLanguageLang(String text, String lang) throws Exception {

        String getText = getText(text);

        RequestSpecification httpRequest = buildBasicRequest(getText,lang,base.projectIdPpx);

        base.response = httpRequest.request(Method.POST, BasePaths.EXTRACTOR_EXTRACT);

    }
}
