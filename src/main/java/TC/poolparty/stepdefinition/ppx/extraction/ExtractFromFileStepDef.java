package TC.poolparty.stepdefinition.ppx.extraction;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import com.poolparty.commonutils.BasePaths;
import com.poolparty.commonutils.StringHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ExtractFromFileStepDef extends CommonObject {

    private CommonObject base;
    public ExtractFromFileStepDef(CommonObject base) {this.base = base;}

    @Given("Extraction model for Eurovoc 4.6 is available")
    public void extractionModelForEurovoc46IsAvailable() {
        //TODO
        // call /extractor/api/projects
    }

    @When("User sends {string} in format {string} to extract file service using language {word}")
    public void userSendsFileToExtractFileService(String filePath, String fileFormat, String language) throws Exception {
        fileFormat = StringHelper.trimStringByString(fileFormat, "\"");

        RestAssured.baseURI = BaseSetup.properties.getPropertyValue("BaseURL");

        RequestSpecification httpRequest = RestAssured.given();

        File file = new File(this.getClass().getClassLoader().getResource(filePath).getFile());
        httpRequest.queryParam("language", language);
        httpRequest.queryParam("projectId", BaseSetup.properties.getPropertyValue("projectIdPpx"));
        httpRequest.multiPart("file", file, fileFormat);

        base.response = httpRequest.request(Method.POST, BasePaths.EXTRACTOR_EXTRACT);
    }

    @And("File extraction result contains concept with prefLabelLanguage {word}, prefLabelValue {string}, conceptUrl {word}, score {float}, frequencyInDocument {int}")
    public void fileExtractionResultContainsConceptWithPrefLabelLanguagePrefLabelValueConceptUrlScoreFrequency(String prefLabelLanguage,
                                                                                                           String prefLabelValue,
                                                                                                           String conceptUrl,
                                                                                                           float score,
                                                                                                           int frequencyInDocument){
        conceptUrl = StringHelper.trimStringByString(conceptUrl, "\"");
        prefLabelLanguage = StringHelper.trimStringByString(prefLabelLanguage, "\"");
        prefLabelValue = StringHelper.trimStringByString(prefLabelValue, "\"");

        List<LinkedHashMap> concepts = (List) base.response.jsonPath().getList("document.concepts");
        for (LinkedHashMap concept : concepts) {
            //when conceptUrl matches .. see if the whole result matches
            String uri_result = (String) concept.get("uri");
            if(uri_result.equals(conceptUrl)){
                Float score_result = (Float) concept.get("score");
                Assert.assertEquals("score should be equal",
                        score_result, score, 0.0);

                Integer frequencyInDocument_result = (Integer) concept.get("frequencyInDocument");
                Assert.assertEquals("frequency in document should be equal",
                        (int) frequencyInDocument_result, frequencyInDocument);

                LinkedHashMap prefLabels = (LinkedHashMap) concept.get("prefLabels");
                String prefLabelsLang_result = (String) prefLabels.get(prefLabelLanguage);
                Assert.assertEquals("prefLabel for lang (" + prefLabelLanguage + ") should be equal",
                        prefLabelsLang_result, prefLabelValue);

                return;
            }
        }

        Assert.fail("No matching concept with expected url '" + conceptUrl + "' found in result.");
    }

    @And("File extraction result contains extracted term with extractedTerm {string}, score {float}, frequencyInDocument {int}")
    public void fileExtractionResultContainsExtractedTermWithExtractedTermScoreFrequencyInDocument(String expectedTerm,
                                                                                                  float score,
                                                                                                  int frequencyInDocument) {
        // remove trailing and final " from feature file
        expectedTerm = StringHelper.trimStringByString(expectedTerm, "\"");

        List<LinkedHashMap> extractedTerms = (List) base.response.jsonPath().getList("document.extractedTerms");

        for (LinkedHashMap extractedTerm : extractedTerms) {
            String extractedTerm_result = (String) extractedTerm.get("textValue");
            if(extractedTerm_result.equals(expectedTerm)){
                Float score_result = (Float) extractedTerm.get("score");
                Assert.assertEquals("score should be equal",
                        score_result, score, 0.0);

                Integer frequencyInDocument_result = (Integer) extractedTerm.get("frequencyInDocument");
                Assert.assertEquals("frequency in document should be equal",
                        (int) frequencyInDocument_result, frequencyInDocument);
                return;
            }
        }

        Assert.fail("No matching extracted term '" + expectedTerm + "' found in result.");
    }
}