package TC.poolparty.stepdefinition.ppx.extraction;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import com.poolparty.commonutils.BasePaths;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import static org.hamcrest.Matchers.equalTo;

public class ExtractFromUrlStepDef extends CommonObject {

    private CommonObject base;

    // common method to build a basic request
    public RequestSpecification buildBasicRequest(String url, String language, String projectID) throws Throwable {

        return RestAssured.
                given().
                header("Content-Type", "application/x-www-form-urlencoded").
                queryParam("url", url).
                queryParam("projectId", projectID).
                queryParam("language", language);
    }

    public ExtractFromUrlStepDef(CommonObject base) {
        this.base = base;
    }

    @Given("Project to get concepts and terms from a given url is available")
    public void projectIsReady() throws Throwable {
        //TODO
        // call /extractor/api/projects
    }

    @When("User sends api call with language {word} and Url {word} to get concepts and terms")
    public void userSendsApiCallWithLanguageAndUrl(String language, String url) throws Throwable {
        RequestSpecification httpRequest = buildBasicRequest(
                url, language, BaseSetup.properties.getPropertyValue("projectIdPpx"));

        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_EXTRACT);
    }

    @When("User sends api call with language {word} and Url {word} using non existing project")
    public void userSendsApiCallWithLanguageAndUrlUsingNonExistingProject(String language, String url) throws Throwable {
        RequestSpecification httpRequest = buildBasicRequest(url, language, "notExistingProjectId");

        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_EXTRACT);
    }

    @When("User sends api call with language {word}, Url {word} with limit for " +
            "concepts {int} and terms {int}")
    public void userSendsApiCallWithLanguageLanguageUrlUrlWithLimitForConceptsLimitConceptsAndTermsLimitTerms(
            String language, String url, int limitConcepts, int limitTerms) throws Throwable {

        RequestSpecification httpRequest = buildBasicRequest(
                url, language, BaseSetup.properties.getPropertyValue("projectIdPpx")).
                queryParam("numberOfConcepts", limitConcepts).
                queryParam("numberOfTerms", limitTerms);

        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_EXTRACT);
    }

    @When("User sends api call with language {word}, Url {word} with limit for concept minimum score of {float}")
    public void userSendsApiCallWithLanguageLanguageUrlUrlWithLimitForConceptMinimumScoreOfConceptMinimumScore(
            String language, String url, float conceptMinimumScore) throws Throwable {

        RequestSpecification httpRequest = buildBasicRequest(
                url, language, BaseSetup.properties.getPropertyValue("projectIdPpx")).
                queryParam("conceptMinimumScore", conceptMinimumScore);

        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_EXTRACT);
    }

    @And("Extracted term of entryIndex {int} is {} with score {float} and frequency {int}")
    public void extractedTermOfEntryIndexIndexIsTextValueWithScoreScoreAndFrequencyFrequencyInDocument(
            int index, String textValue, float score, int frequencyInDocument) {

        base.response.then().assertThat().
                body("extractedTerms[" + index + "].textValue", equalTo(textValue)).
                body("extractedTerms[" + index + "].score", equalTo(score)).
                body("extractedTerms[" + index + "].frequencyInDocument", equalTo(frequencyInDocument));
    }

    @And("Extracted concept of entryIndex {int} in language {word} is {} with id {word}, uri {word}, " +
            "score {float} and frequency {int}")
    public void extractedConceptOfEntryIndexIndexIsPrefLabelsWithIdIdUriUriScoreScoreAndFrequencyFrequencyInDocument(
            int index, String language, String prefLabels, String id, String uri,
            float score, int frequencyInDocument) {

        base.response.then().assertThat().
                body("concepts[" + index + "].prefLabels." + language, equalTo(prefLabels)).
                body("concepts[" + index + "].id", equalTo(projectID + ":" + id)).
                body("concepts[" + index + "].uri", equalTo(uri)).
                body("concepts[" + index + "].score", equalTo(score)).
                body("concepts[" + index + "].frequencyInDocument", equalTo(frequencyInDocument));

    }


}
