package TC.poolparty.stepdefinition.ppx.heartbeat;


import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import com.poolparty.commonutils.BasePaths;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

public class HeartBeatStepDef extends CommonObject {

    private CommonObject base;
    public HeartBeatStepDef(CommonObject base) {this.base = base;}

    @Given("A server is available and Index is configured")
    public void aServerIsAvailableAndIndexIsConfigured() throws Throwable{

    }

    @When("User sends a request with heartbeat API")
    public void userSendsARequestWithHeartbeatAPI() throws Throwable {

           RequestSpecification httpRequest = RestAssured.given().log().all();
        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_HEARTBEAT);

    }

    @When("User sends a request with wrong credentials")
    public void userSendsARequestWithWrongCredentials() throws Throwable {

          RequestSpecification httpRequest = RestAssured.given().auth().
                basic(BaseSetup.properties.getPropertyValue("UserName"), BaseSetup.properties.getPropertyValue("WrongPassword"));

        base.response = httpRequest.request(Method.GET, BasePaths.EXTRACTOR_HEARTBEAT);


    }
}
