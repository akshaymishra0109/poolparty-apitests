package TC.poolparty.stepdefinition.ppx;

import com.poolparty.basesetup.CommonObject;
import cucumber.api.java.en.And;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

public class CommonExtractorStepDefinition extends CommonObject {

    private CommonObject base;
    public CommonExtractorStepDefinition(CommonObject base){
        this.base=base;
    }

    @And("User should get exactly {int} extracted terms from file extraction")
    public void userShouldGetExactlyExtractedTermsFromFileExtraction(int expectedNumber) {
        assertListSize(expectedNumber, "document.extractedTerms");
    }

    @And("User should get exactly {int} extracted concepts from file extraction")
    public void userShouldGetExactlyExtractedConceptsFromFileExtraction(int expectedNumber) {
        assertListSize(expectedNumber, "document.concepts");
    }

    @And("User should get exactly (.*) extracted concepts")
    public void userShouldGetExactlyExtractedConcepts(int expectedNumber) {
        assertListSize(expectedNumber,"concepts");
    }

    @And("User should get exactly (.*) extracted terms")
    public void userShouldGetExactlyExtractedTerms(int expectedNumber) {
        assertListSize(expectedNumber, "extractedTerms");
    }

    private void assertListSize(int expectedSize, String jsonPath) {
        List<Object> jsonElementList = base.response.jsonPath().getList(jsonPath);
        Assert.assertEquals(expectedSize ,jsonElementList.size());
    }

    @And("User should get only extracted terms but no extracted concepts")
    public void userShouldGetOnlyExtractedTerms() {
        assertThat(base.response.path("extractedTerms.textValue"), notNullValue());
        assertThat(base.response.path("concepts.id"), nullValue());
    }

}
