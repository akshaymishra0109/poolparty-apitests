package TC.poolparty.stepdefinition.ppt;

import com.poolparty.basesetup.CommonObject;
import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateProjectStepDef extends CommonObject {

    private CommonObject base;
    public CreateProjectStepDef(CommonObject base) {this.base = base;}

    @Given("A valid ppar file are available")
    public void aValidPparFileAreAvailable() throws Throwable{



    }

    @When("when I create a project with ppar file")
    public void whenICreateAProjectWithPparFile() throws Throwable{
       base.response =  base.createProjectStepLib.createProjectRequest("superadmin", "poolparty","", "", base.scenario);
        
    }

    @And("I validate the project title in response")
    public void iValidateTheProjectTitleInResponse() throws Throwable{
        
    }

    @And("store the project ID")
    public void storeTheProjectID() throws Throwable{
        
    }

}
