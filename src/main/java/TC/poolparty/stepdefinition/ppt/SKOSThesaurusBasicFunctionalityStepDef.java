package TC.poolparty.stepdefinition.ppt;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import com.poolparty.commonutils.Generics;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;

import static com.poolparty.commonutils.Generics.*;

public class SKOSThesaurusBasicFunctionalityStepDef extends CommonObject {
    private CommonObject base;
    public SKOSThesaurusBasicFunctionalityStepDef(CommonObject base) {this.base = base;}

    @Given("A PoolParty project Id is provided")
    public void aPoolPartyProjectIdIsProvided() throws Throwable {
       base.projectID = BaseSetup.properties.getPropertyValue("projectId");
    }

    @When("I sent the request with project ID")
    public void iSentTheRequestWithProjectId() throws Throwable
    {

            RestAssured.baseURI = BaseSetup.properties.getPropertyValue("BaseURL");
            RequestSpecification httpRequest = RestAssured.given().auth().
                    basic(BaseSetup.properties.getPropertyValue("UserName"), BaseSetup.properties.getPropertyValue("Password"));

            base.response = httpRequest.request(Method.GET, "/PoolParty/api/thesaurus/" + projectID + "/schemes");

    }

    @Then("The list of concept schemes should be returned")
     public void theListOfConceptSchemesShouldBeReturned() throws Throwable {
        String  responseBody = base.response.getBody().asString();
     //   base.object= responseBody;
     //   base.mapForStepsStrings.put("responseBdy",responseBody);
      //  System.out.println(" Concept Scheme Response Body is =>  " + responseBody);
        scenario.write("Response is: " + responseBody);

    }

/*    @And("I should receive the status code {int} for the getConceptScheme request")
    public void iShouldReceiveTheStatusCodeForTheGetConceptSchemeRequest(int expectedStatusCode) throws Throwable {
            expectedStatusCode = 200;
     //       base.mapForStepsStrings.get("responseBdy");
           int actualStatusCode = base.response.getStatusCode();
            Assert.assertEquals(*//*actual value*//* actualStatusCode, *//* expected value*//* expectedStatusCode);

    } */

}
