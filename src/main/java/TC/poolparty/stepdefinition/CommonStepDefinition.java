package TC.poolparty.stepdefinition;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.ContentType;
import org.testng.Assert;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class CommonStepDefinition extends CommonObject{

    private CommonObject base;
    public CommonStepDefinition(CommonObject base){
        this.base=base;
    }

    @Then("User should receive a status code of {int}")
    public void userShouldReceiveAStatusCodeOf(int statusCode) throws Throwable{
        base.response.then().assertThat().statusCode(statusCode);
    }

    @Then("I verify the content type of response should be Json")
    public void iVerifyTheContentTypeOfResponseShouldBeJson() throws Throwable {
        base.response.then().spec(base.verification.validateContentTypeForResponseIsJson());
    }

    @Then("Content type is of response type Json-UTF8")
    public void contentTypeIsOfResponseTypeJsonUTF8() throws Throwable {
        Assert.assertEquals("application/json;charset=UTF-8", base.response.contentType());
    }

    @Then("I validate {string} to be {string} in response at Jsonpath {string}")
    public void iValidateToBeInResponseAtJsonpath(String string, String itemName, String jsonPath) {
        base.response.then().body(jsonPath, equalTo(itemName));
    }

    @Then("I validate {string} to be either {string} in response at Jsonpath {string}")
    public void iValidateToBeEitherInResponseAtJsonpath(String string, String expectedValue, String jsonPath) throws Throwable {
        String actualValue;
        actualValue = base.response.then().extract().jsonPath().get(jsonPath);
        if (!expectedValue.contains(actualValue)) {
            throw new Throwable ("Expected Value: "+" "+expectedValue +" "+", Does not contain the actual values:"+" "+actualValue );
        };
    }

    @Then("I should get message {string} in response body")
    public void iShouldGetMessageInResponseBody(String expectedValue) throws Throwable {
        String actualResponseBody;
        actualResponseBody = base.response.then().extract().asString();
        actualResponseBody = actualResponseBody.trim();

        if (!actualResponseBody.contains(expectedValue.trim())) {
            throw new Throwable ("Response Body: "+" "+actualResponseBody +" "+", Does not contain the expectedMessage:"+" "+expectedValue );
        };
    }

    @And("Result should match expected schema {string}")
    public void resultShouldMatchExpectedSchema(String schemaFile){
        String jsonBody = base.response.getBody().asString();
        org.junit.Assert.assertThat(jsonBody, matchesJsonSchemaInClasspath(schemaFile));
    }

    @And("Default user sends request")
    public void defaultUserSendsRequest() throws Exception {
        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(BaseSetup.properties.getPropertyValue("UserName"));
        authScheme.setPassword(BaseSetup.properties.getPropertyValue("Password"));
        RestAssured.authentication = authScheme;

    }

}
