package models;

public class CreateProject {
    private String author;

    private String remoteRepositoryIRI;

    private String subject;

    private String[] availableLanguages;

    private String description;

    private String qualitySetting;

    private String title;

    private String idGeneration;

    private String license;

    private String[] userGroups;

    private String baseURL;

    private String contributor;

    private String defaultLanguage;

    private String workflowState;

    private String repositoryType;

    private String enableWorkflow;

    private String incrementStart;

    private String publisher;

    private String enableSkosXl;

    private String projectIdentifier;

    private String snapshotInterval;

    private String workflowAssignee;

    public String getAuthor ()
    {
        return author;
    }

    public void setAuthor (String author)
    {
        this.author = author;
    }

    public String getRemoteRepositoryIRI ()
    {
        return remoteRepositoryIRI;
    }

    public void setRemoteRepositoryIRI (String remoteRepositoryIRI)
    {
        this.remoteRepositoryIRI = remoteRepositoryIRI;
    }

    public String getSubject ()
    {
        return subject;
    }

    public void setSubject (String subject)
    {
        this.subject = subject;
    }

    public String[] getAvailableLanguages ()
    {
        return availableLanguages;
    }

    public void setAvailableLanguages (String[] availableLanguages)
    {
        this.availableLanguages = availableLanguages;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getQualitySetting ()
    {
        return qualitySetting;
    }

    public void setQualitySetting (String qualitySetting)
    {
        this.qualitySetting = qualitySetting;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getIdGeneration ()
    {
        return idGeneration;
    }

    public void setIdGeneration (String idGeneration)
    {
        this.idGeneration = idGeneration;
    }

    public String getLicense ()
    {
        return license;
    }

    public void setLicense (String license)
    {
        this.license = license;
    }

    public String[] getUserGroups ()
    {
        return userGroups;
    }

    public void setUserGroups (String[] userGroups)
    {
        this.userGroups = userGroups;
    }

    public String getBaseURL ()
    {
        return baseURL;
    }

    public void setBaseURL (String baseURL)
    {
        this.baseURL = baseURL;
    }

    public String getContributor ()
    {
        return contributor;
    }

    public void setContributor (String contributor)
    {
        this.contributor = contributor;
    }

    public String getDefaultLanguage ()
    {
        return defaultLanguage;
    }

    public void setDefaultLanguage (String defaultLanguage)
    {
        this.defaultLanguage = defaultLanguage;
    }

    public String getWorkflowState ()
    {
        return workflowState;
    }

    public void setWorkflowState (String workflowState)
    {
        this.workflowState = workflowState;
    }

    public String getRepositoryType ()
    {
        return repositoryType;
    }

    public void setRepositoryType (String repositoryType)
    {
        this.repositoryType = repositoryType;
    }

    public String getEnableWorkflow ()
    {
        return enableWorkflow;
    }

    public void setEnableWorkflow (String enableWorkflow)
    {
        this.enableWorkflow = enableWorkflow;
    }

    public String getIncrementStart ()
    {
        return incrementStart;
    }

    public void setIncrementStart (String incrementStart)
    {
        this.incrementStart = incrementStart;
    }

    public String getPublisher ()
    {
        return publisher;
    }

    public void setPublisher (String publisher)
    {
        this.publisher = publisher;
    }

    public String getEnableSkosXl ()
    {
        return enableSkosXl;
    }

    public void setEnableSkosXl (String enableSkosXl)
    {
        this.enableSkosXl = enableSkosXl;
    }

    public String getProjectIdentifier ()
    {
        return projectIdentifier;
    }

    public void setProjectIdentifier (String projectIdentifier)
    {
        this.projectIdentifier = projectIdentifier;
    }

    public String getSnapshotInterval ()
    {
        return snapshotInterval;
    }

    public void setSnapshotInterval (String snapshotInterval)
    {
        this.snapshotInterval = snapshotInterval;
    }

    public String getWorkflowAssignee ()
    {
        return workflowAssignee;
    }

    public void setWorkflowAssignee (String workflowAssignee)
    {
        this.workflowAssignee = workflowAssignee;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [author = "+author+", remoteRepositoryIRI = "+remoteRepositoryIRI+", subject = "+subject+", availableLanguages = "+availableLanguages+", description = "+description+", qualitySetting = "+qualitySetting+", title = "+title+", idGeneration = "+idGeneration+", license = "+license+", userGroups = "+userGroups+", baseURL = "+baseURL+", contributor = "+contributor+", defaultLanguage = "+defaultLanguage+", workflowState = "+workflowState+", repositoryType = "+repositoryType+", enableWorkflow = "+enableWorkflow+", incrementStart = "+incrementStart+", publisher = "+publisher+", enableSkosXl = "+enableSkosXl+", projectIdentifier = "+projectIdentifier+", snapshotInterval = "+snapshotInterval+", workflowAssignee = "+workflowAssignee+"]";
    }
}
