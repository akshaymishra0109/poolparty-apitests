package com.poolparty.commonutils;

import com.poolparty.basesetup.BaseSetup;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CucumberReport {

    public static void generateHtmlReport() throws Exception, Error {
        File reportOutputDirectory = new File(BaseSetup.properties.getPropertyValue("CucumberHtmlReport")+"/");
        List<String> jsonFiles = new ArrayList<>();
        jsonFiles.add(BaseSetup.properties.getPropertyValue("CucumberJsonReport")+"/cucumber-json-report.json");

        String buildNumber = "101";
        String projectName = "PoolParty";
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        configuration.setBuildNumber(buildNumber);
        configuration.setSortingMethod(SortingMethod.NATURAL);
        configuration.addPresentationModes(PresentationMode.EXPAND_ALL_STEPS);
        //configuration.setTrendsStatsFile(new File("target/test-classes/demo-trends.json"));
        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
        
    }
}