package com.poolparty.commonutils;

public class BasePaths {
    public static final String EXTRACTOR_EXTRACT = "/extractor/api/extract";
    public static final String EXTRACTOR_HEARTBEAT = "/extractor/api/heartbeat";
}
