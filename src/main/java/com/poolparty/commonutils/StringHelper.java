package com.poolparty.commonutils;

public class StringHelper {
    /**
     * Removes trailing and ending String trimBy from String text.
     * @param text the text that should be trimmed.
     * @param trimBy the character sequence that should be removed from begin and/or end.
     * @return
     */
    public static String trimStringByString(String text, String trimBy) {
        int beginIndex = 0;
        int endIndex = text.length();

        while (text.substring(beginIndex, endIndex).startsWith(trimBy)) {
            beginIndex += trimBy.length();
        }

        while (text.substring(beginIndex, endIndex).endsWith(trimBy)) {
            endIndex -= trimBy.length();
        }

        return text.substring(beginIndex, endIndex);
    }
}
