package com.poolparty.commonutils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JacksonJsonSerializer {
    ObjectMapper mapper;

    public JacksonJsonSerializer() {
        mapper = new ObjectMapper();
    }

    public String getJsonAttributeValue(String json, String jsonPath) throws Throwable {
        String value = null;
        String[] arrayPath;
        int loop;
        JsonNode jsonNode;

        try {
            jsonNode = mapper.readTree(json);
            arrayPath = jsonPath.split("\\.");
            for (loop = 0; loop < arrayPath.length; loop++) {
                jsonNode = jsonNode.get(arrayPath[loop]);
            }
            // value = jsonNode.toString().trim();
            if (jsonNode.isTextual()) {
                value = jsonNode.toString().trim();
                value = value.substring(1, value.length() - 1);
            } else {
                value = jsonNode.toString().trim();
            }
        } catch (Throwable t) {
            throw t;
        }
        return value;
    }

    public List<JsonNode> getJsonArray(String json, String jsonPath) throws Throwable {
        String[] arrayPath;
        int loop;
        JsonNode jsonNode;
        List<JsonNode> jsonArrayList;
        Iterator<JsonNode> itr = null;
        try {
            jsonNode = mapper.readTree(json);
            arrayPath = jsonPath.split("\\.");
            for (loop = 0; loop < arrayPath.length; loop++) {
                if (jsonNode.get(arrayPath[loop]).isArray()) {
                    itr = jsonNode.get(arrayPath[loop]).elements();
                } else {
                    jsonNode = jsonNode.get(arrayPath[loop]);
                }
            }
            jsonArrayList = new ArrayList<JsonNode>();
            while (itr.hasNext()) {
                JsonNode jsonValue = itr.next();
                jsonArrayList.add(jsonValue);
            }
        } catch (Throwable t) {
            throw t;
        }
        return jsonArrayList;
    }

    public List<JsonNode> getJsonArray(String json) throws Throwable {
        JsonNode jsonNode;
        List<JsonNode> jsonArrayList;
        Iterator<JsonNode> itr = null;
        try {
            jsonNode = mapper.readTree(json);
            itr = jsonNode.elements();
            jsonArrayList = new ArrayList<JsonNode>();
            while (itr.hasNext()) {
                JsonNode jsonValue = itr.next();
                jsonArrayList.add(jsonValue);
            }

        } catch (Throwable t) {
            throw t;
        }
        return jsonArrayList;
    }

    public Map convertJsonToMap(String json) throws Throwable {
        try {
            Map<String, Object> map = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
            });
            /*
             * for (Entry<String, Object> entry : map.entrySet()) {
             * System.out.println("Key = " + entry.getKey() + ", Value = " +
             * entry.getValue()); }
             */
            return map;
        } catch (Throwable t) {
            throw t;
        }
    }
}
