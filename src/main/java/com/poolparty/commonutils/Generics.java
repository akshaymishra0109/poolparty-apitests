package com.poolparty.commonutils;

import com.poolparty.basesetup.BaseSetup;
import cucumber.api.Result;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.IOUtils;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import cucumber.api.Scenario;
import org.apache.commons.lang3.reflect.FieldUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import static io.restassured.RestAssured.given;

public class Generics {
    final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";


    /**
     * @author Akshay
     * @summary this method is a precondition for each request
     */

    public static void basicAuth(String userName, String password) throws Throwable{

        RestAssured.baseURI = BaseSetup.properties.getPropertyValue("BaseURL");

        System.out.println("Base url is : " + RestAssured.baseURI);

        userName = BaseSetup.properties.getPropertyValue("UserName");
        password = BaseSetup.properties.getPropertyValue("Password");
        RequestSpecification httpRequest = RestAssured.given().auth().basic(userName, password);

    }

    /**
     * @author Akshay
     * @summary this method will set the content type as Json for request
     */
    public RequestSpecification setContentTypeJsonForRequest() throws Throwable {
        return given().contentType(ContentType.JSON).request();
    }


    /**
     * @author Akshay
     * @Summary this method will fetch jason path from response
     */

    public JsonPath getJsonPath(Response response) throws Throwable {
        String respString;
        try {
            respString = response.asString();
            JsonPath json = new JsonPath(respString);
            return json;
        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this method will fetch the xml path from response
     */
    public XmlPath getXmlPath(Response response) throws Throwable {
        String respString;
        try {
            respString = response.asString();
            XmlPath xml = new XmlPath(respString);
            return xml;
        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this method will converts the Json File content into String
     */
    public String generateResourceFromFiles(String path) throws Throwable {
        String payload;
        InputStream is;
        try {

            is = Generics.class.getResourceAsStream(path);
            payload = IOUtils.toString(is, StandardCharsets.UTF_8);
            //return new String(Files.readAllBytes(Paths.get(path)));
            return payload;
        } catch (Throwable t) {
            throw t;
        }
    }


    /**
     * @author Akshay
     * @Summary this returns the Json Payload into string
     */
    public String getPayLoad(String path) throws Throwable {
        String completePath;
        String modifiedPath = "";
        String[] arrayPath;
        int loop;
        try {
            arrayPath = path.split("/");
            for (loop = 0; loop < arrayPath.length; loop++) {
                modifiedPath = modifiedPath + BaseSetup.pathSeparator + arrayPath[loop];
            }

            completePath =  BaseSetup.pathSeparator + "jsonpayload"
                    + modifiedPath + ".json";


			/*completePath = System.getProperty("user.dir") + BaseSetup.pathSeparator + "src" + BaseSetup.pathSeparator
					+ "test" + BaseSetup.pathSeparator + "resources" + BaseSetup.pathSeparator + "jsonpayload"
					+ BaseSetup.pathSeparator + modifiedPath + ".json";*/
            return generateResourceFromFiles(completePath);

        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this returns json schema content as string, to be used in schema
     *          validation
     */
    public String getJsonSchemaAsString(String path) throws Throwable {
        String completePath;
        String modifiedPath = "";
        String[] arrayPath;
        int loop;
        try {
            arrayPath = path.split("/");
            for (loop = 0; loop < arrayPath.length; loop++) {
                modifiedPath = modifiedPath + BaseSetup.pathSeparator + arrayPath[loop];
            }

            completePath = BaseSetup.pathSeparator + "jsonschema"
                    + modifiedPath + ".json";

			/*completePath = System.getProperty("user.dir") + BaseSetup.pathSeparator + "src" + BaseSetup.pathSeparator
					+ "test" + BaseSetup.pathSeparator + "resources" + BaseSetup.pathSeparator + "jsonschema"
					+ BaseSetup.pathSeparator + modifiedPath + ".json";*/

            return generateResourceFromFiles(completePath);

        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this method will returns json schema as file object , to be used in schema
     *          validation
     */

    public File getJsonSchemaAsFile(String path) throws Throwable {
        String completePath;
        String modifiedPath = "";
        String[] arrayPath;
        int loop;
        try {
            arrayPath = path.split("/");
            for (loop = 0; loop < arrayPath.length; loop++) {
                modifiedPath = modifiedPath + BaseSetup.pathSeparator + arrayPath[loop];
            }


            completePath =  BaseSetup.pathSeparator + "jsonschema"
                    +  modifiedPath + ".json";

			/*completePath = System.getProperty("user.dir") + BaseSetup.pathSeparator + "src" + BaseSetup.pathSeparator
					+ "test" + BaseSetup.pathSeparator + "resources" + BaseSetup.pathSeparator + "jsonschema"
					+ BaseSetup.pathSeparator + modifiedPath + ".json";*/

            return new File(completePath);
        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this method will generates random string of given length
     */
    public String generateRandomString(int stringLength) {

        StringBuffer randStr = new StringBuffer();
        try {
            for (int i = 0; i < stringLength; i++) {

                int number = getRandomNumber();

                char ch = CHAR_LIST.charAt(number);

                randStr.append(ch);

            }
        } catch (Exception | Error e)

        {
            throw e;
        }
        return randStr.toString();

    }

    /**
     * @author Akshay
     * @Summary this method will generates random number
     */
    private int getRandomNumber() {
        int randomInt = 0;
        try {
            Random randomGenerator = new Random();
            randomInt = randomGenerator.nextInt(CHAR_LIST.length());

            if (randomInt - 1 == -1) {
                return randomInt;
            } else {
                return randomInt - 1;
            }
        } catch (Exception | Error e) {
            throw e;
        }
    }

    /**
     * @author Akshay
     * @Summary this method will generates random number
     */
    public int generateRandomInteger(int num) throws Exception, Error {
        int randomNum = 1;
        int temp = 1;
        try {
            Random rand = new Random();
            for (int i = 1; i <= num; i++) {
                temp = temp * 10;

            }
            randomNum = rand.nextInt(temp);

        } catch (Exception | Error e) {
            throw e;
        }
        return randomNum;
    }


    /**
     * @author Akshay
     * @Summary this method will return system date in given format
     */
    public String getCurrentDate(String dateFormat) throws Throwable {
        String dtmCurrentDateInfo;
        try {
            // Current Date Is Generated
            DateFormat dateformat = new SimpleDateFormat(dateFormat);
            Date date = new Date();
            dtmCurrentDateInfo = dateformat.format(date);
        } catch (Throwable t) {
            throw t;
        }
        return dtmCurrentDateInfo;
    }

    public Date convertStringIntoDate(String formate, String givenDate) throws Throwable {
        Date date;
        try {
            date = new SimpleDateFormat(formate).parse(givenDate);
        }catch(Throwable t) {
            throw t;
        }
        return date;
    }

    public String formatDate(Date date, String format) throws Throwable {
        String formatDate;
        try {
            DateFormat dateformat = new SimpleDateFormat(format);
            formatDate = dateformat.format(date);
        }catch (Throwable t) {
            throw t;
        }
        return formatDate;
    }

    /**
     * @author Akshay
     *  @Summary this method will return future date
     */
    public String incrementDate(String dateFormat,int type, int number) throws Throwable {
        String dtmCurrentDateInfo;
        try {
            // Current Date Is Generated
            DateFormat dateformat = new SimpleDateFormat(dateFormat);
            Calendar cal = Calendar.getInstance();
            cal.add(type, number);
            dtmCurrentDateInfo = dateformat.format(cal.getTime());
        } catch (Throwable t) {
            throw t;
        }
        return dtmCurrentDateInfo;
    }

    /**
     * @author Akshay
     * @Summary this returns future date
     */
    public String addDaysToDate(String date, String dateFormat,int type, int number) throws Throwable {
        String dtmCurrentDateInfo;
        try {
            // Current Date Is Generated
            DateFormat dateformat = new SimpleDateFormat(dateFormat);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateformat.parse(date));
            cal.add(type, number);
            dtmCurrentDateInfo = dateformat.format(cal.getTime());
        } catch (Throwable t) {
            throw t;
        }
        return dtmCurrentDateInfo;
    }
    /**
     * @author Akshay
     * @Summary this returns system time in given format
     */
    public String getCurrentTime(String timeFormat) throws Throwable {
        String dtmCurrentTimeInfo;
        try {
            // Current Time Is Generated
            Date date = new Date();
            DateFormat timeformat = new SimpleDateFormat(timeFormat);
            dtmCurrentTimeInfo = timeformat.format(date);
            return dtmCurrentTimeInfo;

        } catch (Throwable t) {
            throw t;
        }
    }

    /**
     * @author Akshay
     * @Summary this provides scenario details, required logs creation
     */
    public static String addScenarioDetails(Scenario scenario) throws Throwable {
        try {

            String scenarioDetails = "*************Above Logs Recorded For Scenario: " + scenario.getName() + ",  "
                    + "Belongs To Feature File :: " + scenario.getId() + "***********";
            scenarioDetails = scenarioDetails + System.lineSeparator() + "And Error is " + " " + logError(scenario);
            return scenarioDetails;
        } catch (Throwable t) {
            throw t;
        }
    }

    private static String logError(Scenario scenario) throws Throwable {
        String errorMessage = null;
        try {
            Field field = FieldUtils.getField((scenario).getClass(), "stepResults", true);
            field.setAccessible(true);
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            for (Result result : results) {
                if (result.getError() != null)
                    errorMessage = result.getError().toString();
            }
        } catch (Throwable t) {
            throw t;
        }
        return errorMessage;
    }





}
