package com.poolparty.commonutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;

public class Verification {

    /**
     * @author Akshay
     * @Summary this validates expected status code in response
     */

    public ResponseSpecification validateExpectedStatus(int validCode) throws Throwable {
        ResponseSpecification  responseSpec;
        try {
            ResponseSpecBuilder builder = new ResponseSpecBuilder();
            builder.expectStatusCode(validCode);
            responseSpec = builder.build();

        }catch (Throwable t) {
            throw t;
        }
        return responseSpec;
    }

    /**
     * @author Akshay
     * @Summary this validates content type to be json type for response
     */
    public ResponseSpecification  validateContentTypeForResponseIsJson() throws Throwable {
        ResponseSpecification  responseSpec;
        try {
            ResponseSpecBuilder builder = new ResponseSpecBuilder();
            builder.expectContentType(ContentType.JSON)	;
            responseSpec = builder.build();

        }catch (Throwable t) {
            throw t;
        }
        return responseSpec;
    }


    public void verifyTwoJsonsEquals(String firstJson, String secondJson) throws Throwable {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Assert.assertEquals(mapper.readTree(firstJson.trim()), mapper.readTree(secondJson.trim()));

        }catch (Throwable t) {
            throw t;
        }
    }

    public void verifyTwoJsonsPartially(String firstJson, String secondJson) throws Throwable {
        try {
            if (firstJson.trim().contains(secondJson.trim())) {

            }else {
                throw new Throwable ("First Json : "+firstJson +", is not contained in second Json : " +secondJson );
            }
        }catch (Throwable t) {
            throw t;
        }
    }
}
