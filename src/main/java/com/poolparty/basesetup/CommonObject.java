package com.poolparty.basesetup;

import com.poolparty.commonutils.Generics;
import com.poolparty.commonutils.JacksonJsonSerializer;
import com.poolparty.commonutils.Verification;
import com.poolparty.steplibrary.CreateProjectStepLib;
import cucumber.api.Scenario;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

public class CommonObject {
    public String stepInfo;
    public Response response;
    public Scenario scenario;
    public Generics generics;
    public Verification verification ;
    public JacksonJsonSerializer jsonSerializer;
    public Map<String,Object> mapForSteps;
    public Map<String,String> mapForStepsStrings;
    public CreateProjectStepLib createProjectStepLib;
    public String projectID;
    public String projectIdPpx;
    public RequestSpecification request;
}
