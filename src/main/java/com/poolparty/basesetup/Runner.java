package com.poolparty.basesetup;

import com.poolparty.commonutils.CucumberReport;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources/featurefiles",
        plugin = {"pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/cucumber-json-report.json",
                "rerun:target/cucumber-reports/rerun.txt"},
        extraGlue = "TC.poolparty.stepdefinition",
       tags = {"@Heartbeat"}
)

public class Runner {
    @BeforeClass
    public static void beforeClass() throws Throwable {
        BaseSetup.getInstance();

    }

    @AfterClass
    public static void afterClass() throws Exception {
        CucumberReport.generateHtmlReport();
           }

}
