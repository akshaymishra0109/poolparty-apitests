package com.poolparty.basesetup;

import com.poolparty.steplibrary.CreateProjectStepLib;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class CucumberHook {

	private CommonObject base;
	public CucumberHook(CommonObject base) {
		this.base = base;
	}

	@Before
	public void beforeScenario(Scenario sc) throws Throwable {
		System.out.println("Scenario is: " + sc.getName());
		base.scenario = sc;
	//	base.scenario.write("Execution Started At:    "+base.generics.getCurrentTime("HH:mm:ss"));
		base.createProjectStepLib = new CreateProjectStepLib();
	}

	@After
	public void afterHook(Scenario sc) throws Throwable {


	//		base.scenario.write("Execution Completed At:    "+base.generics.getCurrentTime("HH:mm:ss"));
			base.createProjectStepLib = null;

	}
}
