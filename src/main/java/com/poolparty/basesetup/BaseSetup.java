package com.poolparty.basesetup;

import java.io.File;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;
import com.poolparty.commonutils.FailureLogs;
import com.poolparty.commonutils.PropertyReader;
import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import io.restassured.config.LogConfig;
import io.restassured.specification.RequestSpecification;

/**
 * Purpose : This is a base class in which before suite and after suite
 * configuration is defined
 *
 * @author Akshay
 *
 */

public class BaseSetup {

    public static String pathSeparator;
    public static PrintStream printStream;
    public static PropertyReader properties;
    private static BaseSetup baseObject = null;

    private BaseSetup() throws Throwable {

        try {

            properties = new PropertyReader("utilities.properties");
            pathSeparator = File.separator;
            pathSeparator = "/";

            if (System.getenv("BaseURL") == null || System.getenv("BaseURL").equals("")) {
                RestAssured.baseURI = properties.getPropertyValue("BaseURL");
            } else {
                RestAssured.baseURI = System.getenv("BaseURL");
            }
            System.out.println("Base url is : " + RestAssured.baseURI);


           // Logs generation
            if (properties.getPropertyValue("FailureLoggingToFile").equalsIgnoreCase("Yes")) {
                printStream = FailureLogs.writeFailureLogsToFile(properties.getPropertyValue("FailureLogsFilePath"));
                RestAssured.config = RestAssured.config()
                        .logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails()
                                .defaultStream(BaseSetup.printStream).enablePrettyPrinting(true));
            } else {
                RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
            }


        } catch (Throwable t) {
            System.out.println("Error Occurred in BaseSetup Constructor is " + t.toString());
        }

    }
    public static BaseSetup getInstance() throws Throwable {

        if (baseObject == null) {
            synchronized (BaseSetup.class) {
                if (baseObject == null) {
                    baseObject = new BaseSetup();// instance will be created at request time
                }
            }
        }
        return baseObject;
    }

    public static void killInstance() throws Throwable {

        baseObject = null;
        properties = null;
        printStream = null;

    }


}
