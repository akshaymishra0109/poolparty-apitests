package com.poolparty.steplibrary;

import com.poolparty.basesetup.BaseSetup;
import com.poolparty.basesetup.CommonObject;
import cucumber.api.Scenario;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;

public class CreateProjectStepLib{
    public Response createProjectRequest(String userName, String password, String repositoryType, String  remoteRepositoryIRI, Scenario scenario) throws Throwable
    {
        RestAssured.baseURI = BaseSetup.properties.getPropertyValue("BaseURL");
        File pparFile = new File("src/main/resources/data/testData/pparFiles/pp_project_classifier.ppar");
        RequestSpecification httpRequest = RestAssured.given().multiPart("file", pparFile).auth().
                basic(BaseSetup.properties.getPropertyValue("UserName"), BaseSetup.properties.getPropertyValue("Password"));
                Response response = httpRequest.request(Method.POST,"/PoolParty/api/projects/create");
                scenario.write("Response is: " + response.asString());
                return response;
              //  System.out.println("Response is: " + response.asString());


    }
}
