package com.poolparty.steplibrary;

import com.poolparty.basesetup.BaseSetup;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CommonUtility {

    public void basicAuth(String userName, String password) throws Throwable
    {

        RestAssured.baseURI = BaseSetup.properties.getPropertyValue("BaseURL");
        userName = BaseSetup.properties.getPropertyValue("UserName");
        password = BaseSetup.properties.getPropertyValue("Password");
        RequestSpecification httpRequest = RestAssured.given().auth().basic(userName, password);

    }
}
