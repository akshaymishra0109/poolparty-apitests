Feature: Get Projects

  Scenario: Retrieve all available projects
    Given A PoolParty server that contains at least one project
    And projects are accessible for given user
    When request projects services
    Then a list of accessible projects is retrieved